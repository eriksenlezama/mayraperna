module.exports = {
    api: {
        port: process.env.API_PORT || 3010,
    },
    jwt: {
        secret: process.env.JWT_SECRET || 'notasecret!'
    },
    mysql: {
        host: process.env.MYSQL_HOST || '127.0.0.1',
        port: process.env.MYSQL_PORT || '3306',
        user: process.env.MYSQL_USER || 'eriksen',
        password: process.env.MYSQL_PASS || 'inin123.',
        database: process.env.MYSQL_DB || 'api_mayra',
    }
}
