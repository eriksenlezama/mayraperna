import { resolve } from 'path'

export default {
    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        title: 'Mayra Perna - Asesora inmobiliaria',
        htmlAttrs: {
            lang: 'es'
        },
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: '' }
        ],
        link: [
            {
                rel: 'icon',
                type: 'image/x-icon',
                href: '/favicon.ico'
            },
            {
                rel: 'preconnect',
                href: 'https://fonts.gstatic.com'
            },
            {
                hid: "canonical",
                rel: "canonical",
                href: "https://mayraperna.com",
            },
        ]
    },

    alias: {
        'images': resolve(__dirname, './assets/images'),
    },

    css: [
        "~assets/css/main.scss",
    ],

    styleResources: {
        scss: [
            '~assets/css/base/*.scss',
            '~assets/css/abstracts/*.scss',
            '~assets/css/mixins/*.scss',
        ]
    },

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [
    ],

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: [
        '@nuxtjs/style-resources',
    ],

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [
    ],

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {
    }
}
